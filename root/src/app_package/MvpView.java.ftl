package ${packageName};

<#if applicationPackage??>
import ${applicationPackage}.features.base.MvpView;
</#if>



public interface ${name}MvpView extends MvpView {
}