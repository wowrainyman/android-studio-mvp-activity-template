package ${packageName};

import android.os.Bundle;
<#if applicationPackage??>
import ${applicationPackage}.R;
import ${applicationPackage}.features.base.BaseActivity;
import ${applicationPackage}.injection.component.ActivityComponent;
</#if>

import javax.inject.Inject;

public class ${name}Activity extends BaseActivity implements ${name}MvpView {

    
    @Inject ${name}Presenter m${name}Presenter;

    @Override
    protected int getLayout() {
        return R.layout.activity_${classToResource(name)};
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        m${name}Presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        m${name}Presenter.detachView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
    }



}